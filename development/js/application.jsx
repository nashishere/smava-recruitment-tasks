import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { SmavaFormView } from './views/SmavaFormView';
import store from './reducers';

const $ = require("jquery");

$(window).ready(() => {
  render(<Provider store={store}><SmavaFormView/></Provider>, $('#application')[0]);
});
