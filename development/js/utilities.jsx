import IBAN from 'iban';
import { ValidationRules } from './constants';

/**
 * This is the class for validation rules.
 */

export default class Utilities {
  static isEmail(string) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(string);
  }

  static isOnlyLetters(string) {
    return /^[a-zA-Z ]+$/.test(string);
  }

  static isIBAN(string) {
    return IBAN.isValid(string);
  }

  static isNotEmpty(string) {
    return string !== undefined && string.trim() != "";
  }

  static validate(data, validators) {
    let result = 0;
    if ((validators & ValidationRules.REQUIRED) && !this.isNotEmpty(data)) result |= ValidationRules.REQUIRED;
    if ((validators & ValidationRules.ONLY_LETTERS) && !this.isOnlyLetters(data)) result |= ValidationRules.ONLY_LETTERS;
    if ((validators & ValidationRules.EMAIL) && !this.isEmail(data)) result |= ValidationRules.EMAIL;
    if ((validators & ValidationRules.IBAN) && !this.isIBAN(data)) result |= ValidationRules.IBAN;

    return result;
  }
}