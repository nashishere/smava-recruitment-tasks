const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: {
    application: [path.resolve(__dirname, 'development/js/application.jsx'), path.resolve(__dirname, 'development/css/styles.less')],
    libraries: ['jquery', 'react', 'react-dom', 'redux', 'react-redux', 'node-uuid', 'iban'],
  },

  output: {
    path: path.join(__dirname, '/production'),
    filename: './js/[name].min.js'
  },

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        loader: 'babel-loader?babelrc=false',
        query: {
          presets: ['react', 'es2015', 'stage-2'],
          plugins: ['transform-runtime']
        },
      },
      {
        test: /\.png$/,
        loader: "file-loader?name=images/[name].[ext]"
      },
      {
        test: /\.jpg$/,
        loader: "file-loader?name=images/[name].[ext]"
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader?mimetype=application/font-woff',
        query: {
          name: 'fonts/[name].[ext]',
          publicPath: '../'
        }
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader?mimetype=application/octet-stream',
        query: {
          name: 'fonts/[name].[ext]',
          publicPath: '../'
        }
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader?mimetype=application/octet-stream',
        query: {
          name: 'fonts/[name].[ext]',
          publicPath: '../'
        }
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader?mimetype=image/svg+xml',
        query: {
          name: 'fonts/[name].[ext]',
          publicPath: '../'
        }
      },
      {
        test: /\.(less)$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          loader: "css-loader!less-loader",
        })
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'libraries',
      filename: './js/libraries.min.js'
    }),

    new webpack.optimize.OccurrenceOrderPlugin(true),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true
      }
    }),

    new ExtractTextPlugin({
      filename: './styles/style.css',
      allChunks: false,
    })
  ],

  resolve: {
    extensions: ['.js', '.jsx']
  }
};