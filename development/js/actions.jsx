import { Actions } from './constants';

/**
 * All the actions is in here. The actions itself is very basic, but these are doing their jobs just fine
 */

const changeFirstNameAction = (dispatch, firstName) => {
  dispatch({
    type: Actions.SET_FIRST_NAME,
    firstName: firstName
  });
};

const changeLastNameAction = (dispatch, lastName) => {
  dispatch({
    type: Actions.SET_LAST_NAME,
    lastName: lastName
  });
};

const changeEmailAction = (dispatch, email) => {
  dispatch({
    type: Actions.SET_EMAIL,
    email: email
  });
};

const addBankAccountAction = (dispatch) => {
  dispatch({
    type: Actions.ADD_BANK_ACCOUNT
  });
};

const changeBankAccountAction = (dispatch, account) => {
  dispatch({
    type: Actions.CHANGE_BANK_ACCOUNT,
    account: account
  });
};

const removeBankAccountAction = (dispatch, account) => {
  dispatch({
    type: Actions.REMOVE_BANK_ACCOUNT,
    account: account
  });
};

const addErrorAction = (dispatch, error) => {
  dispatch({
    type: Actions.ADD_ERROR,
    error: error
  });
};

const clearAllErrorsAction = (dispatch) => {
  dispatch({
    type: Actions.CLEAR_ALL_ERRORS
  });
};

export { changeFirstNameAction, changeLastNameAction, changeEmailAction, addBankAccountAction, changeBankAccountAction, removeBankAccountAction, addErrorAction, clearAllErrorsAction };