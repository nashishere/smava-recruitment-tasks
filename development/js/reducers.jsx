import { combineReducers, createStore } from 'redux';
import { Actions } from './constants';
import UUID from 'node-uuid';

/**
 * This is the main model of the application.
 */

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  bankAccounts: [],

  errors: {}
};

/**
 * All of the reducers is in here. Most of them are very basic, but the model itself is also very basic, so these are doing their jobs just fine
 */

const firstName = (state = initialState.firstName, action) => {
  switch (action.type) {
    case Actions.SET_FIRST_NAME:
      return action.firstName;

    default:
      return state;
  }
};

const lastName = (state = initialState.lastName, action) => {
  switch (action.type) {
    case Actions.SET_LAST_NAME:
      return action.lastName;

    default:
      return state;
  }
};

const email = (state = initialState.email, action) => {
  switch (action.type) {
    case Actions.SET_EMAIL:
      return action.email;

    default:
      return state;
  }
};

const bankAccounts = (state = initialState.bankAccounts, action) => {
  switch (action.type) {
    case Actions.ADD_BANK_ACCOUNT:
      return [...state, {
        id: UUID.v1(),
        iban: '',
        bankName: ''
      }];

    case Actions.CHANGE_BANK_ACCOUNT:
      return state.map(item => {
        if (item.id == action.account.id) {
          return Object.assign({}, item, action.account);
        } else {
          return item;
        }
      });

    case Actions.REMOVE_BANK_ACCOUNT:
      return state.filter(item => {
        if (item.id != action.account.id) return item;
      });

    default:
      return state;
  }
};

const errors = (state = initialState.errors, action) => {
  switch (action.type) {
    case Actions.ADD_ERROR:
      return Object.assign({}, state, action.error);

    case Actions.CLEAR_ALL_ERRORS:
      return {};

    default:
      return state;
  }
};

const reducers = combineReducers({ firstName, lastName, email, bankAccounts, errors });
const store = createStore(reducers);

export default store;