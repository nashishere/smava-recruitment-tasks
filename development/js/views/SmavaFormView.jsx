import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import Utilities from '../utilities';
import { ValidationRules } from '../constants';
import store from '../reducers';
import { changeFirstNameAction, changeLastNameAction, changeEmailAction, addBankAccountAction, addErrorAction, clearAllErrorsAction } from '../actions';
import { SmavaFormBankAccountView } from './SmavaFormBankAccountView';

/**
 * This is the class for rendering the bank account entry forms.
 */
class SmavaFormComponent extends React.Component {
  static propTypes = {};

  constructor(props) {
    super(props);
  }

  render() {
    const { firstName, lastName, email, bankAccounts, errors } = this.props;
    const { changeFirstName, changeLastName, changeEmail, addBankAccount } = this.props;

    let listOfBankAccounts = bankAccounts.map(account => {
      return <SmavaFormBankAccountView key={account.id} account={account} />;
    });

    if ((typeof errors.bankAccounts  === 'string') && listOfBankAccounts.length <= 0) {
      listOfBankAccounts.push(<div key="0" className="col-xs-12 text-center error-text">{errors.bankAccounts}</div>);
    }

    return (
        <div>
          <div className="row">
            <div className="col-xs-12 text-center">
              <h2>Register Account</h2>
            </div>
          </div>

          <form>
            <div className="row">
              <div className="col-xs-12">
                <div className={ errors.firstName ? 'form-group has-error' : 'form-group' }>
                  <label htmlFor="first-name-field">First Name</label>
                  <input type="text" className="form-control" id="first-name-field" name="firstNameField" ref="firstNameField" value={firstName} onChange={event => changeFirstName(event.target.value)} />
                  <span className="help-block">{errors.firstName ? errors.firstName.map((item, index) => <div key={index}>{item}</div>) : ''}</span>
                </div>
              </div>

              <div className="col-xs-12">
                <div className={ errors.lastName ? 'form-group has-error' : 'form-group' }>
                  <label htmlFor="last-name-field">Last Name</label>
                  <input type="text" className="form-control" id="last-name-field" name="lastNameField" ref="lastNameField" value={lastName} onChange={event => changeLastName(event.target.value)} />
                  <span className="help-block">{errors.lastName ? errors.lastName.map((item, index) => <div key={index}>{item}</div>) : ''}</span>
                </div>
              </div>

              <div className="col-xs-12">
                <div className={ errors.email ? 'form-group has-error' : 'form-group' }>
                  <label htmlFor="email-field">Email</label>
                  <input type="email" className="form-control" id="email-field" name="emailField" ref="emailField" value={email} onChange={event => changeEmail(event.target.value)} />
                  <span className="help-block">{errors.email ? errors.email.map((item, index) => <div key={index}>{item}</div>) : ''}</span>
                </div>
              </div>

              <div className="col-xs-12">
                <h3>Bank Accounts</h3>
              </div>

              {listOfBankAccounts}

              <div className="col-xs-12 text-center">
                <a className="btn btn-default" onClick={addBankAccount}>
                  <span className="glyphicon glyphicon-plus" aria-hidden="true"/> Add Bank Account
                </a>
              </div>

              <div className="col-xs-12">
                <hr width="100%" />
              </div>

              <div className="col-xs-12 text-right">
                <a className="btn btn-success" onClick={this.onSubmitButtonClicked.bind(this)}>Submit</a>
              </div>
            </div>
          </form>
        </div>
    );
  }

  /**
   * Validation is not so elegant right now, but i want to do it all by myself without any third party validators.
   * Because of the time was short, i couldn't write more robust and eye-candy validation
   */
  onSubmitButtonClicked() {
    const { addError, clearAllErrors } = this.props;
    const { firstName, lastName, email, bankAccounts } = store.getState();

    let errorFound = false;
    clearAllErrors();

    const errors = {};
    const firstNameErrors = Utilities.validate(firstName, ValidationRules.REQUIRED | ValidationRules.ONLY_LETTERS);
    const lastNameErrors = Utilities.validate(lastName, ValidationRules.REQUIRED | ValidationRules.ONLY_LETTERS);
    const emailErrors = Utilities.validate(email, ValidationRules.REQUIRED | ValidationRules.EMAIL);
    const bankAccountsErrors = {};

    if (firstNameErrors > 0) {
      errorFound = true;
      errors['firstName'] = [];
      if (firstNameErrors & ValidationRules.REQUIRED) errors['firstName'].push('First name is required!');
      if (firstNameErrors & ValidationRules.ONLY_LETTERS) errors['firstName'].push('First name should contain only small and capital letters!');
    }

    if (lastNameErrors > 0) {
      errorFound = true;
      errors['lastName'] = [];
      if (lastNameErrors & ValidationRules.REQUIRED) errors['lastName'].push('First name is required!');
      if (lastNameErrors & ValidationRules.ONLY_LETTERS) errors['lastName'].push('First name should contain only small and capital letters!');
    }

    if (emailErrors > 0) {
      errorFound = true;
      errors['email'] = [];
      if (emailErrors & ValidationRules.REQUIRED) errors['email'].push('Email is required!');
      if (emailErrors & ValidationRules.EMAIL) errors['email'].push('Email is malformatted!');
    }

    if (bankAccounts.length <= 0) {
      errorFound = true;
      errors['bankAccounts'] = 'There must be at least one account!';
    } else {
      bankAccounts.forEach(account => {
        const ibanErrors = Utilities.validate(account.iban, ValidationRules.REQUIRED | ValidationRules.IBAN);
        const bankNameErrors = Utilities.validate(account.bankName, ValidationRules.REQUIRED);
        const accountErrors = {};

        if (ibanErrors > 0) {
          accountErrors['iban'] = [];
          if (ibanErrors & ValidationRules.REQUIRED) accountErrors['iban'].push('IBAN is required!');
          if (ibanErrors & ValidationRules.IBAN) accountErrors['iban'].push('IBAN is malformatted!');
        }

        if (bankNameErrors > 0) {
          accountErrors['bankName'] = [];
          if (bankNameErrors & ValidationRules.REQUIRED) accountErrors['bankName'].push('Bank name is required!');
        }

        if (Object.keys(accountErrors).length > 0) {
          bankAccountsErrors[account.id] = accountErrors;
        }
      });

      if (Object.keys(bankAccountsErrors).length > 0) {
        errorFound = true;
        errors['bankAccounts'] = bankAccountsErrors;
      }
    }

    if (!errorFound) {
      setTimeout(alert, 100, JSON.stringify(store.getState(), (name, value) => {
        if (name == 'bankAccounts') {
          return value.map(item => {
            const { id, ...rest} = item;
            return rest;
          });
        }

        if (name != 'errors') return value;
      }, 4));
    } else {
      addError(errors);
    }
  }
}

const SmavaFormView = connect(
    (state) => {
      return {
        firstName: state.firstName,
        lastName: state.lastName,
        email: state.email,
        bankAccounts: state.bankAccounts,
        errors: state.errors,
      }
    },
    (dispatch, ownProps) => {
      return {
        changeFirstName: firstName => changeFirstNameAction(dispatch, firstName),
        changeLastName: lastName => changeLastNameAction(dispatch, lastName),
        changeEmail: email => changeEmailAction(dispatch, email),
        addBankAccount: () => addBankAccountAction(dispatch),
        addError: error => addErrorAction(dispatch, error),
        clearAllErrors: () => clearAllErrorsAction(dispatch),
      }
    }
)(SmavaFormComponent);

export { SmavaFormView };