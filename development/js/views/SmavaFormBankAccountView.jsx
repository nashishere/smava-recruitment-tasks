import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { changeBankAccountAction, removeBankAccountAction } from '../actions';

/**
 * This is the main class for rendering the form.
 */
class SmavaFormBankAccountComponent extends React.Component {
  static propTypes = {
    account: React.PropTypes.shape({
      id: React.PropTypes.string.isRequired,
      iban: React.PropTypes.string.isRequired,
      bankName: React.PropTypes.string.isRequired
    })
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { id, iban, bankName, errors, removeBankAccount, changeBankAccount } = this.props;

    return (
        <div className="col-xs-12">
          <a className="btn btn-link delete-button" onClick={removeBankAccount}>
            <span className="glyphicon glyphicon-trash" aria-hidden="true"/>
          </a>

          <div className="row">
            <div className="col-xs-12">
              <div className={ (errors && errors.iban) ? 'form-group has-error' : 'form-group' }>
                <label htmlFor="iban-field">IBAN</label>
                <input type="text" className="form-control" id="iban-field" name="iban" ref="iban" value={iban} onChange={event => changeBankAccount({ id, iban: event.target.value, bankName })} />
                <span className="help-block">{(errors && errors.iban) ? errors.iban.map((item, index) => <div key={index}>{item}</div>) : ''}</span>
              </div>
            </div>

            <div className="col-xs-12">
              <div className={ (errors && errors.bankName) ? 'form-group has-error' : 'form-group' }>
                <label htmlFor="bank-name-field">Bank Name</label>
                <input type="text" className="form-control" id="bank-name-field" name="bank-name" ref="bank-name" value={bankName} onChange={event => changeBankAccount({ id, iban, bankName: event.target.value })} />
                <span className="help-block">{(errors && errors.bankName) ? errors.bankName.map((item, index) => <div key={index}>{item}</div>) : ''}</span>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

const SmavaFormBankAccountView = connect(
    (state, ownProps) => {
      const filtered = state.bankAccounts.filter(item => {
        if (item.id == ownProps.account.id) return item;
      });

      return {
        id: ownProps.account.id,
        iban: filtered.length > 0 ? filtered[0].iban : '',
        bankName: filtered.length > 0 ? filtered[0].bankName : '',
        errors: (state.errors.bankAccounts && state.errors.bankAccounts[ownProps.account.id]) || {}
      }
    },
    (dispatch, ownProps) => {
      return {
        changeBankAccount: (account) => changeBankAccountAction(dispatch, account),
        removeBankAccount: () => removeBankAccountAction(dispatch, ownProps.account),
      }
    }
)(SmavaFormBankAccountComponent);

export { SmavaFormBankAccountView };