/**
 * Constants and enums is in here.
 */

class Actions {
  static SET_FIRST_NAME = 'SET_FIRST_NAME';
  static SET_LAST_NAME = 'SET_LAST_NAME';
  static SET_EMAIL = 'SET_EMAIL';
  static ADD_BANK_ACCOUNT = 'ADD_BANK_ACCOUNT';
  static CHANGE_BANK_ACCOUNT = 'CHANGE_BANK_ACCOUNT';
  static REMOVE_BANK_ACCOUNT = 'REMOVE_BANK_ACCOUNT';

  static ADD_ERROR = 'ADD_ERROR';
  static CLEAR_ALL_ERRORS = 'CLEAR_ALL_ERRORS';
}

class ValidationRules {
  static REQUIRED = 1;
  static ONLY_LETTERS = 2;
  static EMAIL = 4;
  static IBAN = 8;
}

export { Actions, ValidationRules };